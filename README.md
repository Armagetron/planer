# Schichtenplaner für das Zauberschloss!
[![pipeline status](https://gitlab.com/Armagetron/planer/badges/master/pipeline.svg)](https://gitlab.com/Armagetron/planer/commits/master)

Der Schichtenplaner wird genutzt, um im [Zauberschloss](https://www.derphoenixorden.de/) die Schichten in der Bibliothek und im Hause Hufflepuff zu planen. Er wird jährlich an den geänderten Stundenplan angepasst.

Seit dem Zauberschloss Jahr 2023 basiert der Planer auf .NET 7.0.

Download für: [Windows](https://gitlab.com/Armagetron/planer/-/jobs/artifacts/master/raw/planer/win_x64.tar.xz?job=build_planer) | [Windows + .NET CORE](https://gitlab.com/Armagetron/planer/-/jobs/artifacts/master/raw/planer/win_x64_dotnet.tar.xz?job=build_planer) | [Linux + .NET CORE](https://gitlab.com/Armagetron/planer/-/jobs/artifacts/master/raw/planer/linux_x64.tar.xz?job=build_planer)

# Benutzung

Das Programm ist ein Kommandozeilenprogramm und erwartet genau einen Parameter zur Eingabe-Datei. Darin werden alle Darsteller mit Name, Alter und Skill-Level gespeichert. Zusätzlich lassen sich für jeden Darsteller Sperrzeiten hinterlegen. Zu diesen Zeiten wird ihm dann keine Schicht zugeteilt.

## Syntax

Die Syntax ist recht simpel. In jeder Zeile wird ein Darsteller vermerkt. Innerhalb einer Zeile fungiert das `;` als Trennzeichen.

Zuerst wird der Name, gefolgt vom Alter und dem Skill-Level (Zahl aus {0; 1; 2}) jeweils getrennt mit einem `;` angegeben. Das kann dann z.B. so aussehen:
```
Max Mustermann;24;2;
Erika Musterfrau;17;1;
```
Soll jetzt für Erika Musterfrau eine Sperrzeit hinterlegt werden, ändert sich das ganze wie folgt:
```
Max Mustermann;24;2;
Erika Musterfrau;17;1;2018-02-06-12-45;2018-02-06-14-00
```
