FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env
WORKDIR /app

COPY planer/planer.csproj ./planer/planer.csproj
COPY Schichtenplaner/Schichtenplaner.csproj ./Schichtenplaner/Schichtenplaner.csproj
RUN cd planer && dotnet restore

COPY planer/*.cs ./planer/
COPY Schichtenplaner/*.cs ./Schichtenplaner/
RUN cd planer && dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/runtime:7.0
WORKDIR /app
COPY --from=build-env /app/planer/out .
ENTRYPOINT ["dotnet", "planer.dll"]
