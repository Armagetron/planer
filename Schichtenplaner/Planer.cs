using System;
using System.Collections.Generic;
using System.Linq;

namespace Schichtenplaner
{
    public class Planer
    {
        protected List<DateTime> Days;
        protected List<Person> Persons;
        protected SortedDictionary<DateTime, Shift> Shifts;

        protected uint iPersonsPerShift;
        protected int iSeed;
        protected Random rnd;

        protected uint iAgeLimit;
        protected uint iPersonsOverAgeLimit;

        protected uint iTotalSkill;

        public Planer()
        {
            Days = new List<DateTime>();
            Persons = new List<Person>();
            Shifts = new SortedDictionary<DateTime, Shift>();

            iPersonsPerShift = 1;
            iSeed = 0;
            rnd = new Random(iSeed);

            iAgeLimit = 0;
            iPersonsOverAgeLimit = 0;

            iTotalSkill = 2;
        }

        public uint AgeLimit
        {
            get
            {
                return iAgeLimit;
            }
            set
            {
                iAgeLimit = value;
            }
        }

        public uint PersonsAboveLimit
        {
            get
            {
                return iPersonsOverAgeLimit;
            }
            set
            {
                iPersonsOverAgeLimit = value;
            }
        }

        public uint PersonsPerShift
        {
            get
            {
                return iPersonsPerShift;
            }
            set
            {
                iPersonsPerShift = value;
            }
        }

        public int Seed
        {
            get
            {
                return iSeed;
            }
            set
            {
                iSeed = value;
                if (iSeed == Int32.MinValue)
                    rnd = new Random();
                else rnd = new Random(iSeed);
            }
        }

        public void SetDateRange(DateTime BeginDay, DateTime EndDay)
        {
            Days.Clear();
            DateTime current = BeginDay.Date;

            do
            {
                Days.Add(current);
                current = current.AddDays(1);
            } while (current <= EndDay.Date);
        }

        public void AddPerson(Person person)
        {
            Persons.Add(person);
        }

        public virtual String GetPlanAsString()
        {
            String str = "";

            for (int i = 0; i < Shifts.Count; i++)
            {
                str += "Schicht am " + Shifts.ElementAt(i).Value.Begin.Date.ToString("d") + " von " + Shifts.ElementAt(i).Value.Begin.TimeOfDay.ToString() + " bis " + Shifts.ElementAt(i).Value.End.TimeOfDay.ToString() + "\n";
                for (int k = 0; k < Shifts.ElementAt(i).Value.Person.Count; k++)
                {
                    str += Shifts.ElementAt(i).Value.Person.ElementAt(k).Name + "/";
                }
                str = str.Substring(0, str.Length - 1);
                str += "\n";
            }

            for (int i = 0; i < Persons.Count; i++)
            {
                //str += Persons[i].Name + " (" + Persons[i].TotalShifts() + ")\n";
                str += Persons[i].Name + " (" + Persons[i].TotalShiftCount() + ";" + Persons[i].TotalShifts() + ")\n";
            }

            str += "\n";

            return str;
        }

        public bool ShiftHasEnoughPersonsAboveLimit(Shift shift)
        {
            int count = 0;

            if (iPersonsOverAgeLimit == 0) return true;

            for (int i = 0; i < shift.Person.Count; i++)
            {
                if (shift.Person.ElementAt(i).Age >= iAgeLimit) count++;
                if (count >= iPersonsOverAgeLimit) break;
            }
            if (count >= iPersonsOverAgeLimit) return true;
            return false;
        }

        public virtual void MakePlan()
        {
            if (iPersonsPerShift > Persons.Count || Days.Count == 0) throw new ArgumentOutOfRangeException();

            if (Shifts.Count > 0)
            {
                Shifts.Clear();
            }

            Shift test = new Shift();
            test.Begin = new DateTime(2014, 2, 6, 12, 0, 0);
            test.End = new DateTime(2014, 2, 6, 18, 0, 0);

            //Persons.ElementAt(0).AddReservedTime(test);

            Person swap;
            int i1, i2;
            for (int i = 0; i < Persons.Count * 3; i++)
            {
                i1 = rnd.Next(0, Persons.Count);
                i2 = rnd.Next(0, Persons.Count);
                swap = Persons[i1];
                Persons[i1] = Persons[i2];
                Persons[i2] = swap;
            }

            List<Person> Candidates = new List<Person>();
            PersonComparer perComp = new PersonComparer();
            PersonAgeComparer perAgeComp = new PersonAgeComparer();
            RandomPersonComparer rndComp = new RandomPersonComparer();

            perAgeComp.Threshold = iAgeLimit;

            foreach (DateTime day in Days)
            {
                TimeSpan span;
                Shift shift;

                //Set start time
                if (day.DayOfWeek == DayOfWeek.Saturday) //Begin at 1400
                {
                    span = new TimeSpan(14, 0, 0);
                }
                else if (day.DayOfWeek == DayOfWeek.Sunday) //Begin at 1240
                {
                    span = new TimeSpan(12, 40, 0);
                }
                else
                {
                    span = new TimeSpan(11, 0, 0);
                }

                while (span.Hours < 17)
                {
                    shift = new Shift();
                    shift.Begin = new DateTime(day.Year, day.Month, day.Day, span.Hours, span.Minutes, span.Seconds);
                    shift.End = new DateTime(day.Year, day.Month, day.Day, Math.Min(span.Hours + 2, 17), (span.Hours + 2 > 17) ? 0 : span.Minutes, span.Seconds);

                    int index;

                    Candidates.Clear();

                    for (int i = 0; i < Persons.Count; i++)
                    {
                        if (!Persons.ElementAt(i).IsConflict(shift))
                            Candidates.Add(Persons.ElementAt(i));
                    }

                    perComp.Date = shift.Begin;
                    perAgeComp.Date = shift.Begin;

                    bool ageComp = false;

                    for (int i = 0; i < iPersonsPerShift; i++)
                    {
                        if (ShiftHasEnoughPersonsAboveLimit(shift)) Candidates.Sort(perComp);
                        else
                        {
                            Candidates.Sort(perAgeComp);
                            ageComp = true;
                        }

                        index = 0;

                        shift.Person.AddLast(Candidates.ElementAt(index));
                        Candidates.ElementAt(index).AddShift(shift);
                        if (Candidates.ElementAt(index).IsConflict(shift))
                        {
                            Candidates.RemoveAt(index);
                        }

                        if (ageComp)
                        {
                            Person tmp;
                            int pos, pos2;
                            for (int k = 0; k < Candidates.Count * 3; k++)
                            {
                                pos = rnd.Next(0, Candidates.Count);
                                pos2 = rnd.Next(0, Candidates.Count);
                                tmp = Candidates[pos];
                                Candidates[pos] = Candidates[pos2];
                                Candidates[pos2] = tmp;
                            }
                            ageComp = false;
                        }
                    }
                    Shifts.Add(shift.Begin, shift);
                    span = new TimeSpan(span.Hours + 2, span.Minutes, span.Seconds);
                }
            }
        }
    }
}
