using System;
using System.Collections.Generic;

namespace Schichtenplaner
{
    public class Shift
    {
        public LinkedList<Person> Person;
        public DateTime Begin;
        public DateTime End;

        public Shift()
        {
            Person = new LinkedList<Person>();
            Begin = DateTime.MinValue;
            End = DateTime.MinValue;
        }

        public Shift(Shift shift)
        {
            Person = new LinkedList<Person>();
            foreach (Person p in shift.Person)
            {
                Person.AddLast(p);
            }
            Begin = shift.Begin;
            End = shift.End;
        }

        public bool IsOverlapped(Shift shift)
        {
            if (this.Begin >= shift.End || this.End <= shift.Begin) return false;

            return true;
        }
    }
}
