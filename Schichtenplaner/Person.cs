using System;
using System.Collections.Generic;

namespace Schichtenplaner
{
    public class Person
    {
        private string szName;
        private uint iAge;
        private uint iSkilled;
        List<Shift> ReservedTimes;
        List<Shift> Shifts;

        public Person(string Name)
        {
            szName = Name;
            iAge = 0;
            iSkilled = 2;
            ReservedTimes = new List<Shift>();
            Shifts = new List<Shift>();
        }

        public Person(string Name, uint Age)
        {
            szName = Name;
            iAge = Age;
            iSkilled = 2;
            ReservedTimes = new List<Shift>();
            Shifts = new List<Shift>();
        }

        public Person(string Name, uint Age, uint Skill)
        {
            szName = Name;
            iAge = Age;
            iSkilled = Skill;
            ReservedTimes = new List<Shift>();
            Shifts = new List<Shift>();
        }

        public string Name
        {
            get
            {
                return szName;
            }
        }

        public uint Age
        {
            get
            {
                return iAge;
            }
        }

        public uint Skill
        {
            get
            {
                return iSkilled;
            }
        }

        public List<Shift> ResTimes
        {
            get
            {
                List<Shift> times = new List<Shift>();
                foreach (Shift shift in ReservedTimes)
                {
                    times.Add(shift);
                }
                return times;
            }
        }

        public bool AddReservedTime(Shift shift)
        {
            if (ReservedTimes.Contains(shift)) return false;

            ReservedTimes.Add(shift);
            return true;
        }

        public bool AddShift(Shift shift)
        {
            if (Shifts.Contains(shift)) return false;

            Shifts.Add(shift);
            return true;
        }

        public void ClearReserved()
        {
            ReservedTimes.Clear();
        }

        public void ClearShifts()
        {
            Shifts.Clear();
        }

        public int TotalShifts()
        {
            TimeSpan span = new TimeSpan(0);

            foreach (Shift shift in Shifts)
            {
                span += shift.End - shift.Begin;
            }

            return (int)span.TotalMinutes;
        }

        public int TotalShiftCount()
        {
            return Shifts.Count;
        }

        public uint HasLongShift()
        {
            uint hasShift = 0;

            foreach (Shift shift in Shifts)
            {
                if ((shift.End - shift.Begin).TotalMinutes >= 140)
                {
                    hasShift++;
                }
            }

            return hasShift;
        }

        public bool HasResAtDay(DateTime day)
        {
            bool res = false;
            foreach (Shift shift in ReservedTimes)
            {
                if (shift.Begin.Date == day.Date)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public int ShiftsAtDay(DateTime day)
        {
            int count = 0;

            foreach (Shift shift in Shifts)
            {
                if (shift.Begin.Date == day.Date) count++;
            }

            return count;
        }

        public bool IsConflict(Shift shift)
        {
            for (int i = 0; i < ReservedTimes.Count; i++)
            {
                if (ReservedTimes[i].IsOverlapped(shift)) return true;
            }

            for (int i = 0; i < Shifts.Count; i++)
            {
                if (Shifts[i].IsOverlapped(shift)) return true;
            }

            return false;
        }

        public override string ToString()
        {
            return Name;
        }

        public enum PersonFormat
        {
            plain,
            storage
        }

        public string ToString(PersonFormat format)
        {
            switch (format)
            {
                case PersonFormat.plain:
                    return Name;
                case PersonFormat.storage:
                    string line = Name + ';' + Age.ToString() + ';' + Skill.ToString();

                    if (ReservedTimes.Count == 0) line += ';';

                    foreach (Shift shift in ReservedTimes)
                    {
                        line += ';' + shift.Begin.ToString("yyyy-MM-dd-HH-mm") + ';' + shift.End.ToString("yyyy-MM-dd-HH-mm");
                    }

                    return line;
            }
            return Name;
        }

        public string GetSkillString()
        {
            switch (Skill)
            {
                case 0:
                    return "Unerfahren";
                case 1:
                    return "Fortgeschritten";
                case 2:
                    return "Erfahren";
            }
            return "Skill nicht bekannt";
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            Person per;

            if (obj is Person)
            {
                per = (Person)obj;

                return (per.Shifts.Count - this.Shifts.Count) * (-1);
            }

            throw new NotImplementedException();
        }

        public int CompareTo(object obj, DateTime day)
        {
            Person per;

            if (obj is Person)
            {
                per = (Person)obj;

                if (per.Shifts.Count - this.Shifts.Count == 0)
                {
                    return (per.ShiftsAtDay(day.Date) - this.ShiftsAtDay(day.Date)) * (-1);
                }

                return (per.Shifts.Count - this.Shifts.Count) * (-1);
            }

            throw new NotImplementedException();
        }

        #endregion
    }

    class PersonComparer : IComparer<Person>
    {
        #region IComparer<Person> Members

        public object Date;

        public virtual int Compare(Person x, Person y)
        {
            /*if (Date != null && Date is DateTime)
            {
                DateTime day = ((DateTime)Date);

                if (x.TotalShifts() - y.TotalShifts() == 0)
                {
                    if (x.ResTimes.Count - y.ResTimes.Count == 0)
                    {
                        return (x.ShiftsAtDay(day.Date) - y.ShiftsAtDay(day.Date));
                    }
                    else return x.ResTimes.Count - y.ResTimes.Count;
                }
            }
            return (x.TotalShifts() - y.TotalShifts());*/
            if (Date != null && Date is DateTime)
            {
                DateTime day = ((DateTime)Date);

                if (x.TotalShifts() - y.TotalShifts() == 0)
                {
                    return (x.ShiftsAtDay(day.Date) - y.ShiftsAtDay(day.Date));
                }
            }
            return (x.TotalShifts() - y.TotalShifts());
        }

        #endregion
    }

    class PersonAgeComparer : PersonComparer
    {
        #region IComparer<Person> Members

        public PersonAgeComparer()
        {
            Date = null;
            Threshold = 0;
        }

        public uint Threshold;

        public override int Compare(Person x, Person y)
        {
            if (Date != null && Date is DateTime)
            {
                DateTime day = ((DateTime)Date);

                if (x.Age >= Threshold && y.Age >= Threshold && x.TotalShifts() - y.TotalShifts() == 0)
                {
                    return (x.ShiftsAtDay(day.Date) - y.ShiftsAtDay(day.Date));
                }
            }

            if (x.Age >= Threshold && y.Age >= Threshold)
            {
                return (x.TotalShifts() - y.TotalShifts());
            }
            else return Convert.ToInt32(y.Age) - Convert.ToInt32(x.Age);
            //return (x.TotalShifts() - y.TotalShifts());
        }

        #endregion
    }

    class PersonSkillComparer : PersonComparer
    {
        #region IComparer<Person> Members

        public PersonSkillComparer()
        {
            Date = null;
            Threshold = 0;
        }

        public uint Threshold;

        public override int Compare(Person x, Person y)
        {
            if (Date != null && Date is DateTime)
            {
                DateTime day = ((DateTime)Date);

                if (x.Skill == 0 && y.Skill == 0 && x.TotalShifts() - y.TotalShifts() == 0)
                {
                    return (x.ShiftsAtDay(day.Date) - y.ShiftsAtDay(day.Date));
                }
            }

            if (x.Skill == 0 && y.Skill == 0)
            {
                return (x.TotalShifts() - y.TotalShifts());
            }
            else if (x.Skill == 0 && y.Skill != 0)
            {
                return -1;
            }
            else if (x.Skill != 0 && y.Skill == 0)
            {
                return 1;
            }
            return 0;
        }

        #endregion
    }

    class RandomPersonComparer : IComparer<Person>
    {
        #region IComparer<Person> Members
        Random rnd;

        public RandomPersonComparer()
        {
            rnd = new Random(1);
        }

        public virtual int Compare(Person x, Person y)
        {
            return rnd.Next(-50, 51);
        }

        #endregion
    }
}
