using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schichtenplaner
{
    public class RandomPlaner : Planer
    {
        public RandomPlaner() : base()
        {
        }

        public virtual String GetPlanAsLatex()
        {
            return "";
        }

        protected virtual TimeSpan GetShiftStartTime(DateTime day)
        {
            TimeSpan span;
            if (day.DayOfWeek == DayOfWeek.Saturday)
            {
                span = new TimeSpan(12, 50, 0);
            }
            else if (day.DayOfWeek == DayOfWeek.Sunday)
            {
                span = new TimeSpan(12, 50, 0);
            }
            else
            {
                span = new TimeSpan(10, 50, 0);
            }
            return span;
        }

        protected virtual TimeSpan GetShiftEndTime(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday:

                    if (date.Hour == 12 && date.Minute == 50 && date.Second == 0)
                        return new TimeSpan(2, 20, 0);

                    if (date.Hour == 15 && date.Minute == 10 && date.Second == 0)
                        return new TimeSpan(1, 50, 0);

                    return new TimeSpan();

                case DayOfWeek.Sunday:
                    if (date.Hour == 12 && date.Minute == 50 && date.Second == 0)
                        return new TimeSpan(2, 20, 0);

                    if (date.Hour == 15 && date.Minute == 10 && date.Second == 0)
                        return new TimeSpan(1, 50, 0);

                    return new TimeSpan();

                default:
                    if (date.Hour == 10 && date.Minute == 50 && date.Second == 0)
                        return new TimeSpan(1, 50, 0);

                    if (date.Hour == 12 && date.Minute == 40 && date.Second == 0)
                        return new TimeSpan(2, 30, 0);

                    if (date.Hour == 15 && date.Minute == 10 && date.Second == 0)
                        return new TimeSpan(1, 50, 0);

                    return new TimeSpan();
            }

            return new TimeSpan();
        }

        public override void MakePlan()
        {
            if (iPersonsPerShift > Persons.Count || Days.Count == 0 || Persons.Count == 0) throw new ArgumentOutOfRangeException();

            if (Shifts.Count > 0)
            {
                Shifts.Clear();
            }

            List<Person> Candidates = new List<Person>();
            List<Person> Selection = new List<Person>();
            PersonComparer perComp = new PersonComparer();
            PersonAgeComparer perAgeComp = new PersonAgeComparer();
            RandomPersonComparer rndComp = new RandomPersonComparer();
            IComparer<Person> comparer;

            perAgeComp.Threshold = iAgeLimit;

            int index;
            bool ageComp = false;

            foreach (DateTime day in Days)
            {
                TimeSpan span;
                Shift shift;

                //Set start time
                span = GetShiftStartTime(day);

                while (span.Hours < 17)
                {
                    shift = new Shift();
                    shift.Begin = new DateTime(day.Year, day.Month, day.Day, span.Hours, span.Minutes, span.Seconds);
                    span = GetShiftEndTime(shift.Begin);
                    shift.End = shift.Begin + span;

                    Candidates.Clear();

                    uint tot_skill = 0;
                    for (int i = 0; i < Persons.Count; i++)
                    {
                        if (!Persons.ElementAt(i).IsConflict(shift))
                        {
                            if(Persons.ElementAt(i).ShiftsAtDay(shift.Begin) == 0)
                            {
                                Candidates.Add(Persons.ElementAt(i));
                                tot_skill += Persons.ElementAt(i).Skill;
                            }
                        }
                    }

                    if(tot_skill < iTotalSkill)
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    perComp.Date = shift.Begin;
                    perAgeComp.Date = shift.Begin;

                    ageComp = false;
                    uint skill_shift = 0;

                    for (int i = 0; i < iPersonsPerShift; i++)
                    {
                        Selection.Clear();
                        if (ShiftHasEnoughPersonsAboveLimit(shift))
                        {
                            comparer = perComp;
                            ageComp = false;
                        }
                        else
                        {
                            comparer = perAgeComp;
                            ageComp = true;
                        }

                        Candidates.Sort(comparer);
                        if (i != 0)
                        {
                            while (Candidates.Count != 0)
                            {
                                if (skill_shift + Candidates[0].Skill < iTotalSkill)
                                {
                                    Candidates.RemoveAt(0);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if (Candidates.Count == 0)
                            {
                                throw new ArgumentOutOfRangeException();
                            }
                            
                        }
                        Selection.Add(Candidates[0]);

                        if (ageComp && Selection[0].Age < iAgeLimit)
                        {
                            throw new ArgumentOutOfRangeException();
                        }
                        for (int k = 1; k < Candidates.Count; k++)
                        {
                            if (ageComp && Candidates[k].Age < iAgeLimit) //drop candidates that are too young
                                continue;

                            if (i >= 1) //drop candidates that would not satisfy the skill level
                            {
                                if (skill_shift + Candidates[k].Skill < iTotalSkill)
                                    continue;
                            }

                            /*if(Candidates[k].HasResAtDay(shift.Begin) && Selection[0].TotalShifts() + 1 >= Candidates[k].TotalShifts())
                            {
                                Selection.Add(Candidates[k]);
                            }
                            else*/ if (Selection[0].TotalShifts() >= Candidates[k].TotalShifts()) //drop candidates with more shifts
                            {
                                if (Candidates[k].ShiftsAtDay(shift.Begin) <= Selection[0].ShiftsAtDay(shift.Begin))
                                    Selection.Add(Candidates[k]);
                            }
                            else break;
                        }
                        if (ageComp)
                        {
                            Selection.Sort(rndComp);
                        }
                        index = rnd.Next(0, Selection.Count);
                        shift.Person.AddLast(Selection.ElementAt(index));
                        skill_shift += Selection.ElementAt(index).Skill;
                        Selection.ElementAt(index).AddShift(shift);
                        Candidates.Remove(Selection.ElementAt(index));
                    }
                    Shifts.Add(shift.Begin, shift);
                    span = new TimeSpan(shift.End.Hour, shift.End.Minute, shift.End.Second);
                }
            }
        }
    }
}
