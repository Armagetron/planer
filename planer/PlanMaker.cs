using Schichtenplaner;
using System;
using System.Collections.Generic;

namespace planer
{
    class PlanMaker
    {
        public static DateTime ParseTime(string szText)
        {
            int year = Convert.ToInt32(szText.Substring(0, szText.IndexOf('-')));
            szText = szText.Substring(szText.IndexOf('-') + 1);
            int month = Convert.ToInt32(szText.Substring(0, szText.IndexOf('-')));
            szText = szText.Substring(szText.IndexOf('-') + 1);
            int day = Convert.ToInt32(szText.Substring(0, szText.IndexOf('-')));
            szText = szText.Substring(szText.IndexOf('-') + 1);
            int hour = Convert.ToInt32(szText.Substring(0, szText.IndexOf('-')));
            szText = szText.Substring(szText.IndexOf('-') + 1);
            int minute = Convert.ToInt32(szText);

            return new DateTime(year, month, day, hour, minute, 0);
        }

        public static List<Person> ProcessFile(Queue<string> lines)
        {
            string line;
            string Name;
            uint Age;
            uint Skill;
            int index;

            bool toBreak;
            List<DateTime> ResTimes = new List<DateTime>();

            List<Person> Persons = new List<Person>();

            while (lines.Count != 0)
            {
                line = lines.Dequeue();

                //Name
                index = line.IndexOf(';');
                if (index != -1) Name = line.Substring(0, index);
                else
                {
                    Persons.Add(new Person(line));
                    continue;
                }
                line = line.Substring(index + 1);

                //Age
                index = line.IndexOf(';');
                if (index != -1) Age = Convert.ToUInt32(line.Substring(0, index));
                else
                {
                    Persons.Add(new Person(Name, Convert.ToUInt32(line)));
                    continue;
                }
                line = line.Substring(index + 1);

                //Skill
                index = line.IndexOf(';');
                if (index != -1) Skill = Convert.ToUInt32(line.Substring(0, index));
                else
                {
                    Persons.Add(new Person(Name, Age, Convert.ToUInt32(line)));
                    continue;
                }
                line = line.Substring(index + 1);

                //Create Person
                Persons.Add(new Person(Name, Age, Skill));

                //Now look for reserved time blocks
                if (line.Length <= 0) toBreak = true;
                else toBreak = false;

                while (!toBreak)
                {
                    index = line.IndexOf(';');
                    if (index == -1)
                    {
                        index = line.Length;
                        toBreak = true;
                    }

                    ResTimes.Add(ParseTime(line.Substring(0, index)));

                    line = line.Substring(Math.Min(index + 1, line.Length));
                }

                if (ResTimes.Count % 2 == 1)
                {
                    throw new ArgumentException();
                }
                else
                {
                    Shift shift = new Shift();
                    for (int k = 0; k < ResTimes.Count; k++)
                    {
                        if (k % 2 == 0)
                        {
                            shift.Begin = ResTimes[k];
                        }
                        else
                        {
                            shift.End = ResTimes[k];
                            Persons[Persons.Count - 1].AddReservedTime(shift);
                            shift = new Shift();
                        }
                    }
                }
                ResTimes.Clear();
            }
            return Persons;
        }
    }
}
