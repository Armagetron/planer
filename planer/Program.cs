using Schichtenplaner;
using System;
using System.Collections.Generic;
using System.IO;

namespace planer
{
    class Program
    {
        static Queue<string> ReadFile(String fName)
        {
            System.IO.StreamReader file = null;
            Queue<string> Lines = new Queue<string>();

            try
            {
                file = new System.IO.StreamReader(fName);
                while (!file.EndOfStream)
                {
                    Lines.Enqueue(file.ReadLine());
                }
                file.Close();
            }
            catch (IOException excep)
            {
                if (file != null) file.Close();

                System.Console.WriteLine("Could not read file!");
#if (DEBUG)
                System.Diagnostics.Debug.WriteLine("Exception caught in function " + excep.TargetSite + ":" + excep.Message);
#endif
            }
            catch (Exception excep)
            {
                if (file != null) file.Close();
#if (DEBUG)
                System.Diagnostics.Debug.WriteLine("Exception caught in function " + excep.TargetSite + ":" + excep.Message);
#endif

            }
            return Lines;
        }

        static void WriteFile(String fName, String content)
        {
            System.IO.StreamWriter file = null;
            Queue<string> Lines = new Queue<string>();

            try
            {
                file = new System.IO.StreamWriter(fName);
                file.WriteLine(content);
                file.Close();
            }
            catch (IOException excep)
            {
                if (file != null) file.Close();

                System.Console.WriteLine("Could not read file!");
#if (DEBUG)
                System.Diagnostics.Debug.WriteLine("Exception caught in function " + excep.TargetSite + ":" + excep.Message);
#endif
            }
            catch (Exception excep)
            {
                if (file != null) file.Close();
#if (DEBUG)
                System.Diagnostics.Debug.WriteLine("Exception caught in function " + excep.TargetSite + ":" + excep.Message);
#endif

            }
        }

        static void Main(string[] args)
        {
            
            Queue<string> Lines = null;
            List<Person> Persons = null;

            //Check parameters
            if(args.Length < 1)
            {
                System.Console.WriteLine("Please specify an input file!");
            }

            Lines = ReadFile(args[0]);
            Persons = PlanMaker.ProcessFile(Lines);
            Planer plan = new RandomPlaner();

            plan.AgeLimit = 0;
            if (plan.AgeLimit != 0)
            {
                plan.PersonsAboveLimit = 1;
            }
            else plan.PersonsAboveLimit = 0;

            for (int i = 0; i < Persons.Count; i++)
            {
                Persons[i].ClearShifts();
                plan.AddPerson(Persons[i]);
            }

            plan.PersonsPerShift = 3;
            plan.Seed = 0;
            plan.SetDateRange(new DateTime(2023, 1, 28), new DateTime(2023, 2, 5));

            try
            {
                plan.MakePlan();
            }
            catch (ArgumentOutOfRangeException excep)
            {
                System.Console.WriteLine("Could not create plan!");
#if (DEBUG)
                System.Diagnostics.Debug.WriteLine("Exception caught in function " + excep.TargetSite + ":" + excep.Message);
#endif
                return;
            }
            System.Console.WriteLine(plan.GetPlanAsString());
            WriteFile(args[0].Substring(0, args[0].Length - 4) + "_Vorschlag.txt", plan.GetPlanAsString());
        }
    }
}
